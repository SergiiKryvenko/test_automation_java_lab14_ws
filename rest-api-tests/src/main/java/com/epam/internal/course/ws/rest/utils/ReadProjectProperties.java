package com.epam.internal.course.ws.rest.utils;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ReadProjectProperties {

    private Properties properties;
    private final String PROPERTIES_PATH = "src\\main\\resources\\project.properties";

    public ReadProjectProperties() {
        properties = new Properties();
        try {
            properties.load(new FileReader(PROPERTIES_PATH));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String getBaseUrl() {
        return properties.getProperty("base.url");
    }

    public String getUriPath() {
        return properties.getProperty("uri.path");
    }
}
