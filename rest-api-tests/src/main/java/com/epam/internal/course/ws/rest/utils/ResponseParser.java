package com.epam.internal.course.ws.rest.utils;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.epam.internal.course.ws.rest.entity.GenreEntity;
import com.epam.internal.course.ws.rest.entity.ResponseCodeEntity;

import io.restassured.response.Response;

public class ResponseParser {

    public static List<GenreEntity> getListOfGenres(Response response) throws ParseException {
        List<GenreEntity> genreEntities = new ArrayList<GenreEntity>();
        if (response.getStatusCode() == 200) {
            GenreEntity genreEntity;
            JSONParser parser = new JSONParser();
            JSONArray array = (JSONArray) parser.parse(response.getBody().asString());
            for (Object genre : array) {
                JSONObject object = (JSONObject) genre;
                genreEntity = new GenreEntity();
                genreEntity.setGenreId(Integer.parseInt((String) object.get("genreId").toString()));
                genreEntity.setGenreName((String) object.get("genreName"));
                genreEntity.setGenreDescription((String) object.get("genreDescription"));
                genreEntities.add(genreEntity);
            }
        } else {
            GenreEntity genreEntity = new GenreEntity();
            genreEntities.add(genreEntity);
        }

        return genreEntities;
    }

    public static GenreEntity getGenre(Response response) {
        GenreEntity genreEntity = new GenreEntity();
        if (response.getStatusCode() == 200 || response.getStatusCode() == 201) {
            genreEntity.setGenreId(response.jsonPath().get("genreId"));
            genreEntity.setGenreName(response.jsonPath().get("genreName"));
            genreEntity.setGenreDescription(response.jsonPath().get("genreDescription"));
        }
        return genreEntity;
    }

    public static ResponseCodeEntity getResponseCode(Response response) {
        return new ResponseCodeEntity(response.getStatusCode());
    }
}
