package com.epam.internal.course.ws.rest.services;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.internal.course.ws.rest.entity.GenreEntity;
import com.epam.internal.course.ws.rest.entity.ResponseCodeEntity;
import com.epam.internal.course.ws.rest.utils.ReadProjectProperties;
import com.epam.internal.course.ws.rest.utils.ResponseParser;

import io.qameta.allure.Step;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class GenreService {

    private static final ReadProjectProperties READ_PROJECT_PROPERTIES = new ReadProjectProperties();
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    private ResponseCodeEntity responseCodeEntity;

    @Step(value = "get a genre by id {0}")
    public GenreEntity getGenreByNumber(int number) {
        logger.info(this.getClass().getSimpleName() + " start getGenreByNumber()");
        RestAssured.baseURI = READ_PROJECT_PROPERTIES.getBaseUrl();
        String uriParameters = "/genre/" + number;
        //
        GenreEntity genreEntity = new GenreEntity();

        // Request object
        RequestSpecification requestSpec = RestAssured.given();

        requestSpec.contentType(ContentType.JSON);// .accept(ContentType.JSON);
        logger.info(this.getClass().getSimpleName() + " preparing Request object for getting a genre by id");

        // Response object
        Response response = requestSpec.request(Method.GET, READ_PROJECT_PROPERTIES.getUriPath() + uriParameters);

        // print response in CLI
        String responseBody = response.getBody().asString();
        logger.info(this.getClass().getSimpleName() + " GET: responseBody = " + responseBody);

        // status code validation
        int statusCode = response.getStatusCode();
        logger.info(this.getClass().getSimpleName() + " GET: statusCode = " + statusCode);
        this.responseCodeEntity = ResponseParser.getResponseCode(response);

        // status line verification
        String statusLine = response.getStatusLine();
        logger.info(this.getClass().getSimpleName() + " GET: statusLine = " + statusLine);
        //
        genreEntity = ResponseParser.getGenre(response);
        //
        return genreEntity;
    }

    @Step(value = "get a list of genres")
    public List<GenreEntity> getGenresList() throws ParseException {
        logger.info(this.getClass().getSimpleName() + " start getGenresList()");
        RestAssured.baseURI = READ_PROJECT_PROPERTIES.getBaseUrl();
        String uriParameters = "/genres";
        //
        List<GenreEntity> genreEntities = new ArrayList<GenreEntity>();

        // Request object
        RequestSpecification requestSpec = new RequestParams()
                .orderTypeByAsc()
                .pages(1)
                .paginationTrue()
                .size(100)
                .sortByGenreId()
                .get();

//        requestSpec = RequestParams.getUriParamsForGenreList(requestSpec);
        requestSpec.contentType(ContentType.JSON);// .accept(ContentType.JSON);
        logger.info(this.getClass().getSimpleName() + " preparing Request object for getting a list of genres");

        // Response object
        Response response = requestSpec.request(Method.GET, READ_PROJECT_PROPERTIES.getUriPath() + uriParameters);

        // print response in CLI
        String responseBody = response.getBody().asString();
        logger.info(this.getClass().getSimpleName() + " GET: responseBody = " + responseBody);

        // status code validation
        int statusCode = response.getStatusCode();
        logger.info(this.getClass().getSimpleName() + " GET: statusCode = " + statusCode);
        this.responseCodeEntity = ResponseParser.getResponseCode(response);

        // status line verification
        String statusLine = response.getStatusLine();
        logger.info(this.getClass().getSimpleName() + " GET: statusLine = " + statusLine);
        //
        genreEntities = ResponseParser.getListOfGenres(response);
        //
        return genreEntities;
    }

    @Step(value = "create a new genre {0}")
    public GenreEntity createNewGenre(GenreEntity genreEntity) {
        logger.info(this.getClass().getSimpleName() + " start createNewGenre()");
        RestAssured.baseURI = READ_PROJECT_PROPERTIES.getBaseUrl();
        String uriParameters = "/genre";
        //
        // Request object
        RequestSpecification requestSpec = RestAssured.given();

        requestSpec.contentType(ContentType.JSON);// .accept(ContentType.JSON);
        requestSpec.accept(ContentType.JSON);
        requestSpec.body(RequestParams.getBodyParamsForCreateUpdateGenre(genreEntity)); // attache data to the request
        logger.info(this.getClass().getSimpleName() + " preparing Request object for creating a new genre");

        // Response object
        Response response = requestSpec.request(Method.POST, READ_PROJECT_PROPERTIES.getUriPath() + uriParameters);

        // print response in CLI
        String responseBody = response.getBody().asString();
        logger.info(this.getClass().getSimpleName() + " GET: responseBody = " + responseBody);

        // status code validation
        int statusCode = response.getStatusCode();
        logger.info(this.getClass().getSimpleName() + " GET: statusCode = " + statusCode);
        this.responseCodeEntity = ResponseParser.getResponseCode(response);

        // status line verification
        String statusLine = response.getStatusLine();
        logger.info(this.getClass().getSimpleName() + " GET: statusLine = " + statusLine);
        //
        genreEntity = ResponseParser.getGenre(response);
        //
        return genreEntity;
    }

    @Step(value = "update a genre {0}")
    public GenreEntity updateGenre(GenreEntity genreEntity) {
        logger.info(this.getClass().getSimpleName() + " start updateGenre()");
        RestAssured.baseURI = READ_PROJECT_PROPERTIES.getBaseUrl();
        String uriParameters = "/genre";
        //
        // Request object
        RequestSpecification requestSpec = RestAssured.given();

        requestSpec.contentType(ContentType.JSON);// .accept(ContentType.JSON);
        requestSpec.accept(ContentType.JSON);
        requestSpec.body(RequestParams.getBodyParamsForCreateUpdateGenre(genreEntity)); // attache data to the request
        logger.info(this.getClass().getSimpleName() + " preparing Request object for updating a genre by id");

        // Response object
        Response response = requestSpec.request(Method.PUT, READ_PROJECT_PROPERTIES.getUriPath() + uriParameters);

        // print response in CLI
        String responseBody = response.getBody().asString();
        logger.info(this.getClass().getSimpleName() + " GET: responseBody = " + responseBody);

        // status code validation
        int statusCode = response.getStatusCode();
        logger.info(this.getClass().getSimpleName() + " GET: statusCode = " + statusCode);
        this.responseCodeEntity = ResponseParser.getResponseCode(response);

        // status line verification
        String statusLine = response.getStatusLine();
        logger.info(this.getClass().getSimpleName() + " GET: statusLine = " + statusLine);
        //
        genreEntity = ResponseParser.getGenre(response);
        //
        return genreEntity;
    }

    @Step(value = "delete a new genre {0}")
    public int deleteGenreById(GenreEntity genreEntity) {
        logger.info(this.getClass().getSimpleName() + " start deleteGenreById()");
        RestAssured.baseURI = READ_PROJECT_PROPERTIES.getBaseUrl();
        String uriParameters = "/genre/" + genreEntity.getGenreId();
        //
        // Request object
        RequestSpecification requestSpec = RestAssured.given();
        requestSpec = RequestParams.getUriParamsForDeleteGenre(requestSpec);
        requestSpec.accept(ContentType.JSON);
        logger.info(this.getClass().getSimpleName() + " preparing Request object for deleting a genre by id");

        // Response object
        Response response = requestSpec.request(Method.DELETE, READ_PROJECT_PROPERTIES.getUriPath() + uriParameters);

        // print response in CLI
        String responseBody = response.getBody().asString();
        logger.info(this.getClass().getSimpleName() + " GET: responseBody = " + responseBody);

        // status code validation
        int statusCode = response.getStatusCode();
        logger.info(this.getClass().getSimpleName() + " GET: statusCode = " + statusCode);
        this.responseCodeEntity = ResponseParser.getResponseCode(response);

        // status line verification
        String statusLine = response.getStatusLine();
        logger.info(this.getClass().getSimpleName() + " GET: statusLine = " + statusLine);
        //
        return statusCode;
    }

    @Step(value = "checking if a genres list {0} contains certain GenreId {0}")
    public boolean checkIfGenreListContainGenreId(List<GenreEntity> genreEntities, int id) {
        logger.info(this.getClass().getSimpleName() + " checking if a genres list contains certain GenreId");
        for (GenreEntity genreEntity : genreEntities) {
            if (genreEntity.getGenreId() == id) {
                return true;
            }
        }
        return false;
    }

    @Step(value = "checking if a genres list contains certain GenreId and correcting new genre id {0}")
    public GenreEntity checkAndCorrectNewGenreID(GenreEntity genreEntity) throws ParseException {
        logger.info(
                this.getClass().getSimpleName() + " checking if a genres list contains certain GenreId and correcting new genre id");
        List<GenreEntity> genres = getGenresList();
        for (GenreEntity genreEntity2 : genres) {
            if (genreEntity.getGenreId() == genreEntity2.getGenreId()) {
                genreEntity.setGenreId(genreEntity.getGenreId() + 1);
                break;
            }
        }
        return genreEntity;
    }
    
    public int getResponseCode() {
        return responseCodeEntity.getResponseCode();
    }
    
}
