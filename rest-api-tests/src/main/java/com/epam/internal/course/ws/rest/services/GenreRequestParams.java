package com.epam.internal.course.ws.rest.services;

public enum GenreRequestParams {

    ORDER_TYPE("orderType"), 
    PAGE("page"), 
    PAGINATION("pagination"), 
    SIZE("size"), 
    SORT_BY("sortBy"), 
    ASC("asc"), 
    DESC("desc"),
    TRUE("true"), 
    FALSE("false"), 
    GENRE_ID("genreId"), 
    GENRE_NAME("genreName"), 
    GENRE_DESCRIPTION("genreDescription"),
    FORCIBLY("forcibly");

    private String param;

    private GenreRequestParams(String param) {
        this.param = param;
    }

    @Override
    public String toString() {
        return param;
    }
}
