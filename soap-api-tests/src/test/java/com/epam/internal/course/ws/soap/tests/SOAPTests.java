package com.epam.internal.course.ws.soap.tests;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.epam.internal.course.ws.soap.data.DataRepository;
import com.epam.internal.course.ws.soap.entity.BookEntity;
import com.epam.internal.course.ws.soap.entity.GenreEntity;
import com.epam.internal.course.ws.soap.services.BookService;
import com.epam.internal.course.ws.soap.services.GenreService;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

public class SOAPTests extends TestRunner {
    
    @DataProvider
    public Object[][] getTestGenre() {
        return new Object[][] { { DataRepository.get().getNewGenre() }, };
    }
    
    @DataProvider
    public Object[][] getUpdatedTestGenre() {
        return new Object[][] { { DataRepository.get().updateNewGenre() }, };
    }

    @Epic("checkQuantityBooks")
    @Feature(value = "Check for getting a list of books")
    @Severity(SeverityLevel.TRIVIAL)
    @Description("Check getting a list of books")
    @Story(value = "Check getting a list of books")
    @Test
    public void checkQuantityBooks() {
        BookService bookService = new BookService();
        List<BookEntity> books = bookService.getBooks();
        this.bookEntity = books.get(0);
        Assert.assertTrue(books.size() > 0, "a list of books doesn't contain any records");
        Assert.assertTrue(books.get(0).getBookId() > 0, "a list of books contains wrong records");
    }
    
    @Epic("checkBookById")
    @Feature(value = "Check for getting a book by id")
    @Severity(SeverityLevel.TRIVIAL)
    @Description("Check for getting a book by id")
    @Story(value = "Check for getting a book by id")
    @Test
    public void checkBookById() {
        BookService bookService = new BookService();
        BookEntity bookEntity = bookService.getBookById(this.bookEntity.getBookId());
        Assert.assertTrue(!bookEntity.getBookName().isEmpty(), "a book contains wrong name");
        Assert.assertEquals(bookEntity, this.bookEntity, "a book contains wrong data");
    }
    
    // negative test
    @Epic("checkWrongBookId")
    @Feature(value = "Negative test. Check for getting a book by a wrong id")
    @Severity(SeverityLevel.TRIVIAL)
    @Description("Check for getting a book by id")
    @Story(value = "Check for getting a book by id")
    @Test
    public void checkWrongBookId() {
        BookService bookService = new BookService();
        BookEntity bookEntity = bookService.getBookById(this.bookEntity.getBookId()-1);
        Assert.assertTrue(bookEntity.getBookName().isEmpty(), "get a success request instead of a wrong");
        Assert.assertTrue(bookService.getResponseErrorMessage().contains("not found"));
    }

    @Epic("checkQuantityGenres")
    @Feature(value = "Check for getting a list of genres")
    @Severity(SeverityLevel.TRIVIAL)
    @Description("Check getting a list of genres")
    @Story(value = "Check getting a list of genres")
    @Test
    public void checkQuantityGenres() {
        GenreService genreService = new GenreService();
        List<GenreEntity> genres = genreService.getGenresList();
        this.genreEntity = genres.get(0);
        Assert.assertTrue(genres.size() > 0, "a list of genres doesn't contain any records");
        Assert.assertTrue(genres.get(0).getGenreId() > 0, "a list of genres contains wrong records");
    }

    @Epic("checkGenreById")
    @Feature(value = "Check for getting a genre by id")
    @Severity(SeverityLevel.TRIVIAL)
    @Description("Check for getting a genre by id")
    @Story(value = "Check for getting a genre by id")
    @Test
    public void checkGenreById() {
        GenreService genreService = new GenreService();
        GenreEntity genreEntity = genreService.getGenreById(this.genreEntity.getGenreId());
        Assert.assertTrue(!genreEntity.getGenreName().isEmpty(), "a genre contains wrong name");
        Assert.assertEquals(genreEntity, this.genreEntity, "a genre contains wrong data");

    }
    
    @Epic("createTestGenre")
    @Feature(value = "Check for creating a TestGenre")
    @Severity(SeverityLevel.TRIVIAL)
    @Description("Check for creating a TestGenre")
    @Story(value = "Check for creating a TestGenre")
    @Test(dataProvider = "getTestGenre")
    public void createTestGenre(GenreEntity genreEntity) {
        GenreService genreService = new GenreService();
        genreEntity = genreService.checkAndCorrectNewGenreID(genreEntity);
        this.genreEntity = genreEntity;
        GenreEntity newGenre = genreService.createGenre(genreEntity);
        Assert.assertTrue(!newGenre.getGenreName().isEmpty(), "a genre contains a wrong name");
        Assert.assertEquals(newGenre, genreEntity, "a genre contains wrong data");
    }
    
    @Epic("updateTestGenre")
    @Feature(value = "Check for updating a TestGenre")
    @Severity(SeverityLevel.TRIVIAL)
    @Description("Check for updating a TestGenre")
    @Story(value = "Check for updating a TestGenre")
    @Test(dataProvider = "getUpdatedTestGenre")
    public void updateTestGenre(GenreEntity newGenreEntity) {
        GenreService genreService = new GenreService();
        newGenreEntity.setGenreId(this.genreEntity.getGenreId());
        GenreEntity genreEntityUpdated = genreService.updateGenre(newGenreEntity);
        Assert.assertTrue(!genreEntityUpdated.getGenreName().isEmpty(), "a genre contains wrong name");
        Assert.assertEquals(newGenreEntity, genreEntityUpdated, "a genre contains wrong data");
    }
    
    @Epic("deleteTestGenre")
    @Feature(value = "Check for deleting a TestGenre")
    @Severity(SeverityLevel.TRIVIAL)
    @Description("Check for deleting a TestGenre")
    @Story(value = "Check for deleting a TestGenre")
    @Test(dataProvider = "getTestGenre")
    public void deleteTestGenre(GenreEntity genreEntity) {
        GenreService genreService = new GenreService();
        genreEntity.setGenreId(this.genreEntity.getGenreId());
        String response = genreService.deleteGenre(genreEntity);
        Assert.assertTrue(response.contains("Successfully deleted genre"), "a request wasn't successful");
    }
    
    // negative test
    @Epic("deleteTestGenreByWrongId")
    @Feature(value = "Negative test. Check for deleting a genre by a wrong id")
    @Severity(SeverityLevel.TRIVIAL)
    @Description("Check for deleting a genre by a wrong id")
    @Story(value = "Check for deleting a genre by a wrong id")
    @Test(dataProvider = "getTestGenre")
    public void deleteTestGenreByWrongId(GenreEntity genreEntity) {
        GenreService genreService = new GenreService();
        genreEntity.setGenreId(this.genreEntity.getGenreId()+1);
        genreService.deleteGenre(genreEntity);
        String response = genreService.getResponseErrorMessage();
        Assert.assertTrue(response.contains("not found")||response.contains("Cannot delete genre"), "a genre deleted successfully");
    }
}
