package com.epam.internal.course.ws.soap.services;

public enum BookEndpoints {

    GET_BOOK("/getBook"), 
    GET_BOOKS("/getBooks");

    private String param;

    private BookEndpoints(String param) {
        this.param = param;
    }

    @Override
    public String toString() {
        return param;
    }
}
