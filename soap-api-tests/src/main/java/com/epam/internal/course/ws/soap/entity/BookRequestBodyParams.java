package com.epam.internal.course.ws.soap.entity;

public enum BookRequestBodyParams {

    GET_BOOKS_REQUEST("getBooksRequest"), 
    GET_BOOK_REQUEST("getBookRequest"),
    SEARCH("search"), 
    ORDER_TYPE("orderType"), 
    PAGE("page"), 
    PAGINATION("pagination"), 
    SIZE("size"), 
    BOOK_ID("bookId");

    private String param;

    private BookRequestBodyParams(String param) {
        this.param = param;
    }

    @Override
    public String toString() {
        return param;
    }
}
