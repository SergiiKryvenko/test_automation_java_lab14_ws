package com.epam.internal.course.ws.soap.entity;

public enum GenreResponseParams {

    GENRE_ID("genreId"), 
    GENRE_NAME("genreName"), 
    GENRE_DESCRIPTION("genreDescription"), 
    GENRE_LANGUAGE("genreLanguage"), 
    GENRE("genre");

    private String param;

    private GenreResponseParams(String param) {
        this.param = param;
    }

    @Override
    public String toString() {
        return param;
    }
}
