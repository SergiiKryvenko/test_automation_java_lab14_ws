package com.epam.internal.course.ws.soap.entity;

public class ResponseErrorMessage {

    private String errorMessage;

    public ResponseErrorMessage() {
        this.errorMessage = "";
    }

    public ResponseErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    @Override
    public String toString() {
        return "ResponseErrorMessage [errorMessage=" + errorMessage + "]";
    }

}
