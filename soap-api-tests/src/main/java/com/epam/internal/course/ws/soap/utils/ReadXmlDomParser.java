package com.epam.internal.course.ws.soap.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.epam.internal.course.ws.soap.entity.BookEntity;

public class ReadXmlDomParser {
    
    private static final String FILENAME = "src\\main\\resources\\message_data.xml";
    //

    private String bookId;
    private String bookName;
    private String bookLanguage;
    private String bookDescription;
    private String pageCount;
    private String height;
    private String width;
    private String length;
    private String publicationYear;
    //
    private List<BookEntity> messageDataList;

    public ReadXmlDomParser() {
        
    }

    public List<BookEntity> getListOfBooks() {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        messageDataList = new ArrayList<BookEntity>();
        BookEntity bookEntity;

        try {

            // optional, but recommended
            // process XML securely, avoid attacks like XML External Entities (XXE)
            dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);

            // parse XML file
            DocumentBuilder db = dbf.newDocumentBuilder();

            Document doc = db.parse(new File(FILENAME));

            // optional, but recommended
            // http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
            doc.getDocumentElement().normalize();

//            System.out.println("Root Element :" + doc.getDocumentElement().getNodeName());
//            System.out.println("------");

            // get <books>
            NodeList listBooks = doc.getElementsByTagName("ns2:book");

            for (int temp = 0; temp < listBooks.getLength(); temp++) {

//                System.out.println("book " + (temp + 1));
                Node node = listBooks.item(temp);

                if (node.getNodeType() == Node.ELEMENT_NODE) {

                    Element element = (Element) node;

                    bookEntity = new BookEntity();

                    //
//                    System.out.println(node.getChildNodes().item(4).getChildNodes().item(1).getChildNodes().item(0).getTextContent());

                    // get Book attributes
                    bookId = node.getChildNodes().item(0).getTextContent();
                    bookName = node.getChildNodes().item(1).getTextContent();
                    bookLanguage = node.getChildNodes().item(2).getTextContent();
                    bookDescription = node.getChildNodes().item(3).getTextContent();
                    publicationYear = node.getChildNodes().item(5).getTextContent();
                    pageCount = node.getChildNodes().item(4).getChildNodes().item(0).getTextContent();
                    height = node.getChildNodes().item(4).getChildNodes().item(1).getChildNodes().item(0).getTextContent();
                    width = node.getChildNodes().item(4).getChildNodes().item(1).getChildNodes().item(1).getTextContent();
                    length = node.getChildNodes().item(4).getChildNodes().item(1).getChildNodes().item(2).getTextContent();

                    //
                    bookEntity.setBookDescription(bookDescription);
                    bookEntity.setBookId(Integer.parseInt(bookId));
                    bookEntity.setBookLanguage(bookLanguage);
                    bookEntity.setBookName(bookName);
                    bookEntity.setHeight(Double.parseDouble(height));
                    bookEntity.setWidth(Double.parseDouble(width));
                    bookEntity.setLength(Double.parseDouble(length));
                    bookEntity.setPageCount(Integer.parseInt(pageCount));
                    bookEntity.setPublicationYear(Integer.parseInt(publicationYear));
                    //
//                    System.out.println("bookId: " + bookId);
//                    System.out.println("bookName: " + bookName);
//                    System.out.println("bookLanguage: " + bookLanguage);
//                    System.out.println("bookDescription: " + bookDescription);
//                    System.out.println("publicationYear: " + publicationYear);
//                    System.out.println("pageCount: " + pageCount);
//                    System.out.println("height: " + height);
//                    System.out.println("width: " + width);
//                    System.out.println("length: " + length);
//                    System.out.println("");
                    //
                    messageDataList.add(bookEntity);
                }
            }

        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        
        return messageDataList;
    }
}
