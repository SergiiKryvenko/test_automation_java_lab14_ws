package com.epam.internal.course.ws.soap.services;

public enum GenreEndpoints {

    GET_GENRES("/getGenres"), 
    GET_GENRE("/getGenre"), 
    CREATE_GENRE("/createGenre"), 
    UPDATE_GENRE("/updateGenre"), 
    DELETE_GENRE("/deleteGenre");

    private String param;

    private GenreEndpoints(String param) {
        this.param = param;
    }

    @Override
    public String toString() {
        return param;
    }
}
