package com.epam.internal.course.ws.soap.utils;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.internal.course.ws.soap.entity.BookEntity;
import com.epam.internal.course.ws.soap.entity.BookResponseParams;
import com.epam.internal.course.ws.soap.entity.ErrorResponseParams;
import com.epam.internal.course.ws.soap.entity.GenreEntity;
import com.epam.internal.course.ws.soap.entity.GenreResponseParams;
import com.epam.internal.course.ws.soap.services.SOAPRequest;

public class RsponseParser {
    //
    protected final static Logger logger = LoggerFactory.getLogger(SOAPRequest.class);
    //

    public static BookEntity getBook(String response) {
        BookEntity bookEntity = new BookEntity();
        if (isRequestSuccess(response)) {
            bookEntity.setBookId(Integer.parseInt(RegexUtils.getDataByTag(response, BookResponseParams.BOOK_ID.toString())));
            bookEntity.setBookName(RegexUtils.getDataByTag(response, BookResponseParams.BOOK_NAME.toString()));
            bookEntity.setBookDescription(RegexUtils.getDataByTag(response, BookResponseParams.BOOK_DESCRIPTION.toString()));
            bookEntity.setBookLanguage(RegexUtils.getDataByTag(response, BookResponseParams.BOOK_LANGUAGE.toString()));
        }
        return bookEntity;
    }
    
    public static GenreEntity getGenre(String response) {
        GenreEntity genreEntity = new GenreEntity();
        if (isRequestSuccess(response)) {
            genreEntity.setGenreId(Integer.parseInt(RegexUtils.getDataByTag(response, GenreResponseParams.GENRE_ID.toString())));
            genreEntity.setGenreName(RegexUtils.getDataByTag(response, GenreResponseParams.GENRE_NAME.toString()));
            genreEntity.setGenreDescription(RegexUtils.getDataByTag(response, GenreResponseParams.GENRE_DESCRIPTION.toString()));
        }
        return genreEntity;
    }
    
    public static List<BookEntity> getListOfBooks(String response) {
        List<BookEntity> bookEntities = new ArrayList<BookEntity>();
        BookEntity bookEntity;
        if (isRequestSuccess(response)) {
            List<String> listBooks = RegexUtils.getListOfDataByTag(response, BookResponseParams.BOOK.toString());
            for (String book : listBooks) {
                bookEntity = new BookEntity();
                bookEntity.setBookId(Integer.parseInt(RegexUtils.getDataByTag(book, BookResponseParams.BOOK_ID.toString())));
                bookEntity.setBookName(RegexUtils.getDataByTag(book, BookResponseParams.BOOK_NAME.toString()));
                bookEntity.setBookDescription(RegexUtils.getDataByTag(book, BookResponseParams.BOOK_DESCRIPTION.toString()));
                bookEntity.setBookLanguage(RegexUtils.getDataByTag(book, BookResponseParams.BOOK_LANGUAGE.toString()));
                bookEntities.add(bookEntity);
            }
        }
        return bookEntities;
    }
    
    public static List<GenreEntity> getListOfGenre(String response) {
        List<GenreEntity> genreEntities = new ArrayList<GenreEntity>();
        GenreEntity genreEntity;
        if (isRequestSuccess(response)) {
            List<String> listGenres = RegexUtils.getListOfDataByTag(response, GenreResponseParams.GENRE.toString());
            for (String genre : listGenres) {
                genreEntity = new GenreEntity();
                genreEntity.setGenreId(Integer.parseInt(RegexUtils.getDataByTag(genre, GenreResponseParams.GENRE_ID.toString())));
                genreEntity.setGenreName(RegexUtils.getDataByTag(genre, GenreResponseParams.GENRE_NAME.toString()));
                genreEntity.setGenreDescription(RegexUtils.getDataByTag(genre, GenreResponseParams.GENRE_DESCRIPTION.toString()));
                genreEntities.add(genreEntity);
            }
        }
        return genreEntities;
    }
    
    private static boolean isRequestSuccess(String response) {
        logger.info(SOAPRequest.class.getSimpleName() + " start isRequestSuccess()");
        if (response.contains(ErrorResponseParams.FAULT_STRING.toString())) {
            logger.error(SOAPRequest.class.getSimpleName() + " The request didn't successful. Response: " + RegexUtils.getErrorText(response));
            return false;
        }
        return true;
    }
    
    public static String getResponseErrorMessage(String response) {
        return RegexUtils.getDataByTag(response, ErrorResponseParams.FAULT_STRING.toString());
    }
}
