package com.epam.internal.course.ws.soap.utils;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ReadProjectProperties {

    private Properties properties;
    private final String PROPERTIES_PATH = "src\\main\\resources\\project.properties";

    public ReadProjectProperties() {
        properties = new Properties();
        try {
            properties.load(new FileReader(PROPERTIES_PATH));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String getSoapEndpointUrl() {
        return properties.getProperty("soap.endpoint.url");
    }

    public String getMessageDataFilePath() {
        return properties.getProperty("message.data.xml.path");
    }

    public String getMyNamespace() {
        return properties.getProperty("my.namespace");
    }

    public String getMyNamespaceUri() {
        return properties.getProperty("my.namespace.uri");
    }
}
